package cz.janousek.advenofcode2021;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class Solver {

	public String solveFirstPart(String inputFile) {
		throw new UnsupportedOperationException();
	}

	public String solveSecondPart(String inputFile) {
		throw new UnsupportedOperationException();
	}

	public abstract String getInputFolder();

	public void solve() {
		log.info("Result of 1st part is {}", solveFirstPart(getInputFile()));
		log.info("Result of 2nd part is {}", solveSecondPart(getInputFile()));
	}

	private String getInputFile() {
		return getInputFolder() + "/input.txt";
	}
}
