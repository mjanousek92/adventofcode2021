package cz.janousek.advenofcode2021.assignment01;

import java.util.List;
import java.util.stream.Collectors;

import cz.janousek.advenofcode2021.Solver;
import cz.janousek.advenofcode2021.utils.LineReader;

public class Solver01 extends Solver {

	@Override
	public String getInputFolder() {
		return "01";
	}

	public String solveFirstPart(String inputFile) {
		final List<Long> numbers = LineReader.readResourceLines(inputFile).stream()
				.map(Long::valueOf)
				.collect(Collectors.toList());

		long previous = numbers.get(0);
		int counter = 0;
		for (int i = 1; i < numbers.size(); i++) {
			long actual = numbers.get(i);
			if (actual > previous) {
				counter++;
			}
			previous = actual;
		}

		return String.valueOf(counter);
	}

	public String solveSecondPart(String inputFile) {
		final List<Long> numbers = LineReader.readResourceLines(inputFile).stream()
				.map(Long::valueOf)
				.collect(Collectors.toList());

		long minus3 = numbers.get(0);
		long minus2 = numbers.get(1);
		long minus1 = numbers.get(2);
		long preiousSum = minus1+minus2+minus3;
		int counter = 0;
		for (int i = 3; i < numbers.size(); i++) {
			long actualSum = numbers.get(i) + numbers.get(i-1) + numbers.get(i-2);
			if (actualSum > preiousSum) {
				counter++;
			}
			preiousSum = actualSum;
		}

		return String.valueOf(counter);
	}
}
