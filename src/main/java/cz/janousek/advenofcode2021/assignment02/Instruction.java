package cz.janousek.advenofcode2021.assignment02;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class Instruction {

	private int value;

	public static Instruction parse(String s) {
		String[] parts = s.split(" ");
		String type = parts[0];
		int value = Integer.valueOf(parts[1]);
		return switch (type) {
			case "forward" -> new ForwardInstruction(value);
			case "down" -> new DownInstruction(value);
			case "up" -> new UpInstruction(value);
			default -> throw new UnsupportedOperationException();
		};
	}

	public abstract void solve(Position position);

	public abstract void solve(PositionWithAim position);

	public static class ForwardInstruction extends Instruction{
		public ForwardInstruction(int value) {
			super(value);
		}

		@Override
		public void solve(Position position) {
			position.updateX(this.getValue());
		}

		@Override
		public void solve(PositionWithAim position) {
			position.updateX(this.getValue());
			position.updateY(position.getAim() * this.getValue());
		}
	}

	public static class DownInstruction extends Instruction{
		public DownInstruction(int value) {
			super(value);
		}

		@Override
		public void solve(Position position) {
			position.updateY(this.getValue());
		}

		@Override
		public void solve(PositionWithAim position) {
			position.updateAim(this.getValue());
		}
	}

	public static class UpInstruction extends Instruction{
		public UpInstruction(int value) {
			super(value);
		}

		@Override
		public void solve(Position position) {
			position.updateY(- this.getValue());
		}

		@Override
		public void solve(PositionWithAim position) {
			position.updateAim(- this.getValue());
		}
	}
}
