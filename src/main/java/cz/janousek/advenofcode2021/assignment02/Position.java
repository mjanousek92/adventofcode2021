package cz.janousek.advenofcode2021.assignment02;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Position {
	private int x;
	private int y;

	public void updateX(int value) {
		this.x += value;
	}

	public void updateY(int value) {
		this.y += value;
	}
}
