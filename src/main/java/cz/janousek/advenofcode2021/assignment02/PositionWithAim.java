package cz.janousek.advenofcode2021.assignment02;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class PositionWithAim {
	private int x;
	private int y;
	private int aim;

	public void updateX(int value) {
		this.x += value;
	}

	public void updateY(int value) {
		this.y += value;
	}

	public void updateAim(int value) {
		this.aim += value;
	}
}
