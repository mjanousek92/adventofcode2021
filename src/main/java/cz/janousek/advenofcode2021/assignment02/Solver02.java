package cz.janousek.advenofcode2021.assignment02;

import java.util.List;
import java.util.stream.Collectors;

import cz.janousek.advenofcode2021.Solver;
import cz.janousek.advenofcode2021.utils.LineReader;

public class Solver02 extends Solver {

	@Override
	public String getInputFolder() {
		return "02";
	}

	public String solveFirstPart(String inputFile) {
		final List<Instruction> instructions = LineReader.readResourceLines(inputFile).stream()
				.map(Instruction::parse)
				.collect(Collectors.toList());

		Position position = new Position(0, 0);
		for (Instruction instruction : instructions) {
			instruction.solve(position);
		}

		return String.valueOf(position.getX() * position.getY());
	}

	public String solveSecondPart(String inputFile) {
		final List<Instruction> instructions = LineReader.readResourceLines(inputFile).stream()
				.map(Instruction::parse)
				.collect(Collectors.toList());

		PositionWithAim position = new PositionWithAim(0, 0, 0);
		for (Instruction instruction : instructions) {
			instruction.solve(position);
		}

		return String.valueOf(position.getX() * position.getY());
	}
}
