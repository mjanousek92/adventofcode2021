package cz.janousek.advenofcode2021.assignment03;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.function.Predicate;

import cz.janousek.advenofcode2021.Solver;
import cz.janousek.advenofcode2021.utils.LineReader;

public class Solver03 extends Solver {

	@Override
	public String getInputFolder() {
		return "03";
	}

	public String solveFirstPart(String inputFile) {
		final List<String> lines = LineReader.readResourceLines(inputFile);

		StringBuilder gamma = new StringBuilder();
		StringBuilder epsilon = new StringBuilder();
		int columns = lines.get(0).length();
		for (int columnIndex = 0; columnIndex < columns; columnIndex++) {
			int zeroCount = 0;
			int oneCount = 0;
			for (String line : lines) {
				char ch = line.charAt(columnIndex);
				if (ch == '0') {
					zeroCount++;
				} else {
					oneCount++;
				}
			}
			if (oneCount > zeroCount) {
				gamma.append("1");
				epsilon.append("0");
			} else {
				gamma.append("0");
				epsilon.append("1");
			}
		}

		return String.valueOf(Integer.parseInt(gamma.toString(), 2) * Integer.parseInt(epsilon.toString(), 2));
	}

	public String solveSecondPart(String inputFile) {
		final List<String> lines = LineReader.readResourceLines(inputFile);

		String oxygen = getLineByPredicate(lines, (zeroCount, oneCount) -> oneCount >= zeroCount);

		String co2 = getLineByPredicate(lines, (zeroCount, oneCount) -> zeroCount > oneCount);

		return String.valueOf(Integer.parseInt(oxygen, 2) * Integer.parseInt(co2, 2));
	}

	private String getLineByPredicate(List<String> lines, BiPredicate<Integer, Integer> predicate) {
		List<String> oxygenLines = new LinkedList<>(lines);
		int columns = oxygenLines.get(0).length();
		for (int columnIndex = 0; columnIndex < columns; columnIndex++) {
			int zeroCount = 0;
			int oneCount = 0;
			LinkedList<String> linesWithOnes = new LinkedList<>();
			LinkedList<String> linesWithZeros = new LinkedList<>();
			for (String line : oxygenLines) {
				char ch = line.charAt(columnIndex);
				if (ch == '0') {
					zeroCount++;
					linesWithZeros.add(line);
				} else {
					oneCount++;
					linesWithOnes.add(line);
				}
			}
			oxygenLines = predicate.test(zeroCount, oneCount) ? linesWithOnes : linesWithZeros;

			if (oxygenLines.size() == 1) {
				break;
			}
		}
		return oxygenLines.get(0);
	}
}
