package cz.janousek.advenofcode2021.assignment04;

import java.util.List;
import java.util.stream.Collectors;

import cz.janousek.advenofcode2021.Solver;
import cz.janousek.advenofcode2021.utils.LineReader;

public class Solver04 extends Solver {

	@Override
	public String getInputFolder() {
		return "template";
	}

	public String solveFirstPart(String inputFile) {
		final List<String> lines = LineReader.readResourceLines(inputFile).stream()
				.collect(Collectors.toList());

		return String.valueOf(lines.size());
	}

	public String solveSecondPart(String inputFile) {
		final List<String> lines = LineReader.readResourceLines(inputFile).stream()
				.collect(Collectors.toList());

		return String.valueOf(lines.size());
	}

}
