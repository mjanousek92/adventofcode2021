package cz.janousek.advenofcode2021.template;

import java.util.LinkedList;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.stream.Collectors;

import cz.janousek.advenofcode2021.Solver;
import cz.janousek.advenofcode2021.utils.LineReader;

public class SolverTemplate extends Solver {

	@Override
	public String getInputFolder() {
		return "template";
	}

	public String solveFirstPart(String inputFile) {
		final List<String> lines = LineReader.readResourceLines(inputFile).stream()
				.collect(Collectors.toList());

		return String.valueOf(lines.size());
	}

	public String solveSecondPart(String inputFile) {
		final List<String> lines = LineReader.readResourceLines(inputFile).stream()
				.collect(Collectors.toList());

		return String.valueOf(lines.size());
	}

}
