package cz.janousek.advenofcode2021.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LineReader {
    public static List<String> readResourceLines(final String input) {
        final Resource resource = new ClassPathResource(input);
        try (Stream<String> stream = Files.lines(resource.getFile().toPath())) {
            return stream.collect(Collectors.toList()); // TODO return stream directly
        } catch (IOException e) {
            log.error("Cannot read file.", e);
            return List.of();
        }
    }
}
