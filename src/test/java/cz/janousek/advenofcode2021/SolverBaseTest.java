package cz.janousek.advenofcode2021;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class SolverBaseTest {
	private Solver solver;

	protected abstract Solver createAssignmentSolver();
	protected abstract String getExpectedFistPartResult();
	protected abstract String getExpectedSecondPartResult();

	public Solver getSolver() {
		return solver;
	}

	@BeforeEach
	void init() {
		solver = createAssignmentSolver();
	}

	@Test
	void solveFirstPart() {
		var actual = getSolver().solveFirstPart(getTestInputFile());
		log.debug("[Test 1 part]: '{}'", actual);
		assertThat(actual).isEqualTo(getExpectedFistPartResult());
	}

	@Test
	void solveSecondPart() {
		var actual = getSolver().solveSecondPart(getTestInputFile());
		log.debug("[Test 2 part]: '{}'", actual);
		assertThat(actual).isEqualTo(getExpectedSecondPartResult());
	}

	@Test
	void testSolve() {
		getSolver().solve();
	}

	private String getTestInputFile() {
		return solver.getInputFolder() + "/test.txt";
	}
}
