package cz.janousek.advenofcode2021.assignment01;

import cz.janousek.advenofcode2021.Solver;
import cz.janousek.advenofcode2021.SolverBaseTest;

class Solver01Test extends SolverBaseTest {

	@Override
	protected Solver createAssignmentSolver() {
		return new Solver01();
	}

	@Override
	protected String getExpectedFistPartResult() {
		return "7";
	}

	@Override
	protected String getExpectedSecondPartResult() {
		return "5";
	}
}