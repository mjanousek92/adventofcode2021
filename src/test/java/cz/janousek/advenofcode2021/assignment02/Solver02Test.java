package cz.janousek.advenofcode2021.assignment02;

import cz.janousek.advenofcode2021.Solver;
import cz.janousek.advenofcode2021.SolverBaseTest;

class Solver02Test extends SolverBaseTest {

	@Override
	protected Solver createAssignmentSolver() {
		return new Solver02();
	}

	@Override
	protected String getExpectedFistPartResult() {
		return "150";
	}

	@Override
	protected String getExpectedSecondPartResult() {
		return "900";
	}
}