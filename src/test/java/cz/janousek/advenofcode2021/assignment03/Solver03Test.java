package cz.janousek.advenofcode2021.assignment03;

import cz.janousek.advenofcode2021.Solver;
import cz.janousek.advenofcode2021.SolverBaseTest;
import cz.janousek.advenofcode2021.assignment02.Solver02;

class Solver03Test extends SolverBaseTest {

	@Override
	protected Solver createAssignmentSolver() {
		return new Solver03();
	}

	@Override
	protected String getExpectedFistPartResult() {
		return "198";
	}

	@Override
	protected String getExpectedSecondPartResult() {
		return "230";
	}
}