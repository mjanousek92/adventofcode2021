package cz.janousek.advenofcode2021.assignment04;

import cz.janousek.advenofcode2021.Solver;
import cz.janousek.advenofcode2021.SolverBaseTest;
import cz.janousek.advenofcode2021.template.SolverTemplate;

class Solver04Test extends SolverBaseTest {

	@Override
	protected Solver createAssignmentSolver() {
		return new Solver04();
	}

	@Override
	protected String getExpectedFistPartResult() {
		return "3";
	}

	@Override
	protected String getExpectedSecondPartResult() {
		return "3";
	}
}