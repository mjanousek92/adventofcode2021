package cz.janousek.advenofcode2021.template;

import static org.junit.jupiter.api.Assertions.*;

import cz.janousek.advenofcode2021.Solver;
import cz.janousek.advenofcode2021.SolverBaseTest;
import cz.janousek.advenofcode2021.assignment02.Solver02;

class SolverTemplateTest extends SolverBaseTest {

	@Override
	protected Solver createAssignmentSolver() {
		return new SolverTemplate();
	}

	@Override
	protected String getExpectedFistPartResult() {
		return "3";
	}

	@Override
	protected String getExpectedSecondPartResult() {
		return "3";
	}
}